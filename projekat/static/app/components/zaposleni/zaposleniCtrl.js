(function (angular) {
    var app = angular.module("app");
    
    app.controller("ZaposleniCtrl", ["$http", function ($http) {
        var that = this;
        this.zaposleni = [];

        this.noviZaposleni = {
            Ime: '',
            Prezime: '',
            zaposlen:true,
            prebivaliste:'',
            drzava:''
        }

        this.dobavljanje_zaposlenih = function() {
            $http.get("/zaposleni").then(function(response){
                that.zaposleni = response.data;
            }, function(response) {
                console.log("Greska pri dobavljanju zaposlenih! Kod: " + response.status);
            })
        }
        this.otpustanje_zaposlenog = function(Id) {
            $http.delete("/zaposleni/"+Id).then(function(response){
                that.dobavljanje_zaposlenih();
            }, function(response){
                console.log("Greska pri otpustanja zaposlenog! Kod: " + response.status);
            });
        }

        this.dodavanje_zaposlenog = function() {
            $http.post("/zaposleni", that.noviZaposleni).then(function(response){
                that.dobavljanje_zaposlenih();
            }, function(){
                console.log("Greska pri dodavanju zaposlenog! Kod: " + response.status);
            });
        }
        this.dobavljanje_zaposlenih();
    }]);
})(angular);