(function(angular){

    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

        $stateProvider.state({
            name: "zaposleni",
            url: "/zaposleni",
            templateUrl: "/app/components/zaposleni/zaposleni.tpl.html",
            controller: "ZaposleniCtrl",
            controllerAs: "zc"
        });

        $urlRouterProvider.otherwise("/");
    }]);
})(angular);