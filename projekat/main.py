import flask
import datetime
from flask import Flask
from flask import request,jsonify
from flaskext.mysql import MySQL
from pymysql.cursors import DictCursor

mysql_db = MySQL(cursorclass=DictCursor)



app = Flask(__name__, static_url_path="")



app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'  
app.config['MYSQL_DATABASE_DB'] = 'kompanija'
app.config['JSON_AS_ASCII'] = False


mysql_db.init_app(app)

@app.route("/")
@app.route("/index")
def index():
    return app.send_static_file("index.html") 




@app.route("/zaposleni", methods=["POST"])
def dodavanje_zaposlenog():
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("INSERT INTO zaposleni (Ime, Prezime,zaposlen,prebivaliste,drzava) VALUES(%(Ime)s, %(Prezime)s,%(zaposlen)s, %(prebivaliste)s,%(drzava)s)", request.get_json())
    db.commit()
    return "", 201

@app.route("/zaposleni", methods=["GET"])
def dobavljanje_zaposlenih():
    cr = mysql_db.get_db().cursor()
    cr.execute("SELECT * FROM zaposleni")
    zaposleni = cr.fetchall()
   
    return flask.json.jsonify(zaposleni)

@app.route("/zaposleni/<int:Id>", methods=["DELETE"])
def otpustanje_zaposlenog(Id):
    db = mysql_db.get_db()
    cr = db.cursor()
    cr.execute("UPDATE zaposleni SET zaposlen=FALSE WHERE Id=%s", (Id,))
    db.commit()
    return "", 204




if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)

