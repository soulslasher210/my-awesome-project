-- MySQL Script generated by MySQL Workbench
-- 04/03/19 14:13:43
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`zaposleni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`zaposleni` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Ime` VARCHAR(45) NOT NULL,
  `Prezime` VARCHAR(45) NOT NULL,
  `zaposlen` BOOLEAN NOT NULL,
  `datum_rodjenja` DATE NOT NULL,
  `prebivaliste` VARCHAR(45) NOT NULL,
  `drzava` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`nadredjeni`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`nadredjeni` (
  `Id_nadredjenog` INT NOT NULL AUTO_INCREMENT,
  `Ime` VARCHAR(45) NOT NULL,
  `Prezime` VARCHAR(45) NOT NULL,
  `datum_rodjenja` DATE NOT NULL,
  `prebivaliste` VARCHAR(45) NOT NULL,
  `drzava` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id_nadredjenog`),
  UNIQUE INDEX `Id_nadredjenog_UNIQUE` (`Id_nadredjenog` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`radno_mesto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`radno_mesto` (
  `Id_radnog_mesta` INT NOT NULL AUTO_INCREMENT,
  `Naziv` VARCHAR(45) NOT NULL,
  `nadredjeni_Id_nadredjenog` INT NOT NULL,
  `zaposleni_Id` INT NOT NULL,
  PRIMARY KEY (`Id_radnog_mesta`),
  UNIQUE INDEX `idradno_mesto_UNIQUE` (`Id_radnog_mesta` ASC),
  UNIQUE INDEX `Naziv_UNIQUE` (`Naziv` ASC),
  INDEX `fk_radno_mesto_nadredjeni1_idx` (`nadredjeni_Id_nadredjenog` ASC),
  INDEX `fk_radno_mesto_zaposleni1_idx` (`zaposleni_Id` ASC),
  UNIQUE INDEX `nadredjeni_Id_nadredjenog_UNIQUE` (`nadredjeni_Id_nadredjenog` ASC),
  UNIQUE INDEX `zaposleni_Id_UNIQUE` (`zaposleni_Id` ASC),
  CONSTRAINT `fk_radno_mesto_nadredjeni1`
    FOREIGN KEY (`nadredjeni_Id_nadredjenog`)
    REFERENCES `mydb`.`nadredjeni` (`Id_nadredjenog`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_radno_mesto_zaposleni1`
    FOREIGN KEY (`zaposleni_Id`)
    REFERENCES `mydb`.`zaposleni` (`Id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;



-- -----------------------------------------------------
-- Data for table `kompanija`.`zaposleni`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`zaposleni` (`Id`, `Ime`, `Prezime`,`zaposlen`,`datum_rodjenja`,`prebivaliste`,`drzava`) 
VALUES (1,'peter','jefferson',TRUE,'1968-01-09','new york','usa');
INSERT INTO `mydb`.`zaposleni` (`Id`, `Ime`, `Prezime`,`zaposlen`,`datum_rodjenja`,`prebivaliste`,`drzava`) 
VALUES (2,'george','sanders',TRUE,'1954-02-01','los angeles','usa');
INSERT INTO `mydb`.`zaposleni` (`Id`, `Ime`, `Prezime`,`zaposlen`,`datum_rodjenja`,`prebivaliste`,`drzava`) 
VALUES (3,'mark','peterson',TRUE,'1970-11-11','san francisco','usa');

COMMIT;


-- -----------------------------------------------------
-- Data for table `kompanija`.`nadredjeni`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`nadredjeni` (`Id_nadredjenog`, `Ime`, `Prezime`,`datum_rodjenja`,`prebivaliste`,`drzava`) 
VALUES (1,'Ted','Jordan','1970-06-05','new york','usa');
INSERT INTO `mydb`.`nadredjeni` (`Id_nadredjenog`, `Ime`, `Prezime`,`datum_rodjenja`,`prebivaliste`,`drzava`) 
VALUES (2,'Kevin','Smith','1951-06-17','los angeles','usa');
INSERT INTO `mydb`.`nadredjeni` (`Id_nadredjenog`, `Ime`, `Prezime`,`datum_rodjenja`,`prebivaliste`,`drzava`) 
VALUES (3,'Harry','Larry','1949-08-06','san francisco','usa');

COMMIT;


-- -----------------------------------------------------
-- Data for table `kompanija`.`radno_mesto`
-- -----------------------------------------------------
START TRANSACTION;
USE `mydb`;
INSERT INTO `mydb`.`radno_mesto` (`Id_radnog_mesta`,`Naziv`,`nadredjeni_Id_nadredjenog`,`zaposleni_Id`)  
VALUES (1, 'Elektrotehnika', 1, 1);
INSERT INTO `mydb`.`radno_mesto` (`Id_radnog_mesta`,`Naziv`,`nadredjeni_Id_nadredjenog`,`zaposleni_Id`) 
VALUES (2, 'IT', 2, 2);
INSERT INTO `mydb`.`radno_mesto` (`Id_radnog_mesta`,`Naziv`,`nadredjeni_Id_nadredjenog`,`zaposleni_Id`) 
VALUES (3, 'Racunovodstvo', 3, 3);

COMMIT;
